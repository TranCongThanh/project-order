var mongoose = require('mongoose');

var orders = new mongoose.Schema({
  userOrder: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  createDate: {
    type: Date,
    default: new Date(),
  },
  desireDate: {
    type: Date,
  },
  status: {
    type: String,
    enum: ['CREATED', 'PROCESSING', 'DONE', 'PAYING']
  },
  amount: {
    type: Number,
    default: 1
  },
  height: {
    type: String
  },
  width: {
    type: String,
  },
  note: {
    type: String
  },
  title: {
    type: String
  }
})

module.exports = orders;